# Variant 3
Create chat dialog UI.

<img src="/img/web-ui-task3.png" width="250" />

Use next resources: <br />
`<link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">` <br />

Background image: https://picsum.photos/320/200?random=1&blur=2 <br />
Avatar 1: https://randomuser.me/api/portraits/lego/1.jpg <br />
Avatar 2: https://randomuser.me/api/portraits/lego/2.jpg

```
:root {
 --lightGray: #dde1e2;
 --darkGray: #d3d7d8;
 --darkBlue: #2589cc;
}
```
**Tips**: You can use https://jsfiddle.net/ to solving this task.

### Your solution:
Create ui-task-v3.html with your solution and commit to Master.