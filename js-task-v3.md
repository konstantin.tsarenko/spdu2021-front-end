# Variant 3

Write a function every_nth to get every nth element in a given array.

`console.log(every_nth([1, 2, 3, 4, 5, 6], 1));` <br />
`[1, 2, 3, 4, 5, 6]`

`console.log(every_nth([1, 2, 3, 4, 5, 6], 2));` <br />
`[2, 4, 6]`

`console.log(every_nth([1, 2, 3, 4, 5, 6], 3));` <br />
`[3, 6]`

`console.log(every_nth([1, 2, 3, 4, 5, 6], 4));` <br />
`[4]`

**Tips**: You can use https://jsfiddle.net/ to solving this task.

### Your solution:
Create js-task-v3.js with your solution and commit to Master.